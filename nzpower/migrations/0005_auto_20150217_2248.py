# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nzpower', '0004_auto_20150216_2148'),
    ]

    operations = [
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, blank=True)),
                ('text', models.TextField(blank=True)),
                ('image', models.ImageField(upload_to=b'nzpower-steps')),
                ('order', models.IntegerField(default=1000)),
                ('active', models.BooleanField(default=True)),
                ('company', models.ForeignKey(to='nzpower.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='company',
            options={'ordering': ['order', 'slug'], 'verbose_name_plural': 'companies'},
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nzpower', '0002_auto_20150213_0047'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='logo',
            field=models.ImageField(max_length=200, null=True, upload_to=b'nzpower-logos', blank=True),
            preserve_default=True,
        ),
    ]

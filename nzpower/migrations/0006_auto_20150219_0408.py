# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nzpower', '0005_auto_20150217_2248'),
    ]

    operations = [
        migrations.CreateModel(
            name='Forward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('src', models.URLField()),
                ('dst', models.URLField()),
                ('when', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(to='nzpower.Company')),
            ],
            options={
                'ordering': ['-when'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='step',
            options={'ordering': ['company', 'order', 'id']},
        ),
    ]

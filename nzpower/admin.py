from django.contrib import admin
from . import models as m

class StepAdmin(admin.ModelAdmin):
    model = m.Step
    list_display = ['title', 'company', 'text', 'order', 'active']
    search_fields = ['title', 'text']
    
admin.site.register(m.Step, StepAdmin)


class StepInline(admin.TabularInline):
    model = m.Step
    extra = 0
    list_display = ['title', 'company', 'text', 'order', 'active']


class CompanyAdmin(admin.ModelAdmin):
    model = m.Company
    inlines = [StepInline]
    list_display = ['name', 'slug', 'admin_logo', 'site', 'bank_account_name', 'bank_account_no', 'order', 'active']

    def admin_logo(self, obj):
        return '<img src="%s" width="50px" />' % obj.logo.url
    admin_logo.allow_tags = True

admin.site.register(m.Company, CompanyAdmin)


class ForwardAdmin(admin.ModelAdmin):
    model = m.Forward
    list_display = ['src', 'dst', 'company', 'when']
    list_filter = ['company']
    
admin.site.register(m.Forward, ForwardAdmin)

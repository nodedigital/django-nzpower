from django.db import models
from slugify import slugify
from django.conf import settings
from os.path import abspath, dirname
from path import path
from urlparse import urljoin
APP_ROOT = path(dirname(abspath(__file__)))
APP_NAME = APP_ROOT.name

def get_full_url(url):
    return urljoin(settings.SITE_URL, url)

class Company(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(unique=True, blank=True)
    site = models.URLField(blank=True, null=True)
    rate = models.IntegerField(default=50)
    bank = models.CharField(max_length=100, default='anz')
    bank_account_name = models.CharField(max_length=100, blank=True, null=True)
    bank_account_no = models.CharField(max_length=30, blank=True, null=True)
    logo = models.ImageField(upload_to='nzpower-logos', max_length=200, blank=True, null=True)
    order = models.IntegerField(default=1000)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['order', 'slug']
        verbose_name_plural = "companies"

    def __unicode__(self):
        return self.name

    def get_short_link(self):
        return "http://payonti.me/%s" % self.slug

    def get_logo_full_url(self):
        return get_full_url(self.logo.url)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        self.slug = self.slug.lower()
        super(Company, self).save(*args, **kwargs)


class Step(models.Model):
    company = models.ForeignKey(Company)
    title = models.CharField(max_length=100, blank=True)
    text = models.TextField(blank=True)
    image = models.ImageField(upload_to='nzpower-steps')
    order = models.IntegerField(default=1000)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['company', 'order', 'id']

    def __unicode__(self):
        return self.title


class Forward(models.Model):
    src = models.URLField()
    dst = models.URLField()
    company = models.ForeignKey(Company)
    when = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-when']

    def __unicode__(self):
        return self.src

